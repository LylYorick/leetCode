package leetcode.editor.cn.startAndJoin;

/**
 * 测试 join是否为并行
 */
public class TestJoinIsParallel {
    public static void main(String[] args) throws InterruptedException {
        String mainThreadName = Thread.currentThread().getName();
        System.out.println(mainThreadName + "start!");
        Thread t1 = new Thread(()->{
            String t1ThreadName = Thread.currentThread().getName();
            System.out.println(t1ThreadName + "start!");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t1ThreadName + "end!");
        });
        Thread t2 = new Thread(()->{
            String t2ThreadName = Thread.currentThread().getName();
            System.out.println(t2ThreadName + "start!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t2ThreadName + "end!");
        });
        Thread t3 = new Thread(()->{
            String t3ThreadName = Thread.currentThread().getName();
            System.out.println(t3ThreadName + "start!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t3ThreadName + "end!");
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(mainThreadName + " running!");
        t3.start();
        t3.join();
        System.out.println(mainThreadName + " end!");
    }
}
