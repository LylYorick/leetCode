package leetcode.editor.cn.startAndJoin;

/**
 * 测试 主线程里面join多个子线程 ，子线程的执行顺序
 */
public class BuildStartAndJoin {
    public static void main(String[] args) throws InterruptedException {
        String mainThreadName = Thread.currentThread().getName();
        System.out.println(mainThreadName + "start!");
        Thread t1 = new Thread(()->{
            String t1ThreadName = Thread.currentThread().getName();
            System.out.println(t1ThreadName + "start!");
//            for (int i = 0; i < 1000 ; i++) {
//                System.out.println(t1ThreadName + "runing !" + i);
//                try {
//                    Thread.sleep((long) (Math.random() * 10));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t1ThreadName + "end!");
        });
        Thread t2 = new Thread(()->{
            String t2ThreadName = Thread.currentThread().getName();
            System.out.println(t2ThreadName + "start!");
//            for (int i = 0; i < 1000 ; i++) {
//                System.out.println(t2ThreadName + "runing !" + i);
//                try {
//                    Thread.sleep((long) (Math.random() * 10));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t2ThreadName + "end!");
        });
        Thread t3 = new Thread(()->{
            String t3ThreadName = Thread.currentThread().getName();
            System.out.println(t3ThreadName + "start!");
//            for (int i = 0; i < 1000 ; i++) {
//                System.out.println(t3ThreadName + "runing !" + i);
//                try {
//                    Thread.sleep((long) (Math.random() * 10));
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t3ThreadName + "end!");
        });
        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println(mainThreadName + " end!");
    }

}
