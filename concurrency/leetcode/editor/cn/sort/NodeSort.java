package leetcode.editor.cn.sort;

public class NodeSort {
     static class Node {
        public int val;
        public Node next;
        public Node(int val){
            this.val = val;
        }
        public Node(int val,Node node){
            this.val = val;
            this.next = node;
        }
    }


    public static Node dedupe(Node head) {
        int temp = head.val;
        Node node = head.next;
        Node parent = head;

        while(node != null){
            if(temp != node.val){
                temp = node.val;
                parent = node;
            } else {
                parent.next = node.next;
            }
            node = node.next;
        }
        return head;
    }

    public static void main(String[] args) {

        Node head = new NodeSort.Node(1);
        Node node = head;
        node.next =  new NodeSort.Node(2);
        node = node.next;
        node.next =  new NodeSort.Node(2);
        node = node.next;

        node.next =  new NodeSort.Node(3);
        node = node.next;
        node.next =  new NodeSort.Node(4);
        node = node.next;
        node.next =  new NodeSort.Node(5);
        node = node.next;
        node.next =  new NodeSort.Node(5);
        node = node.next;
        node.next =  new NodeSort.Node(6);

        Node result = dedupe(head);

        while(result != null){
            System.out.print(result.val + " ");
            result = result.next;
        }

    }

}
