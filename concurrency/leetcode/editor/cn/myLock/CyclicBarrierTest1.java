package leetcode.editor.cn.myLock;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierTest1 {
   static CyclicBarrier c = new CyclicBarrier(7,()->{
       System.out.println("集齐七龙珠 召唤神龙");
   });
    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            final int temp = i;
            new Thread(()->{
                System.out.println("收集到第" + temp + "颗龙珠！");
                try {
                    c.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }

    }

}
