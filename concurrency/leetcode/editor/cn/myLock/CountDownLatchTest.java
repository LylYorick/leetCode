package leetcode.editor.cn.myLock;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        for (int i = 0; i < 5 ; i++) {
            new Thread(new RunName(i,countDownLatch)).start();
        }
        countDownLatch.await();
        System.out.println("主线程执行结束。。。。");
    }

    static class RunName implements Runnable{
        private int id;
        private CountDownLatch countDownLatch;

        public RunName(int id, CountDownLatch countDownLatch) {
            this.id = id;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            System.out.println("线程" + id + "开始！");
            int t = (int)(Math.random()*10 * 1000);
            try {
                Thread.sleep(t);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程" + id  +"在"+ t+"毫秒准备结束");
            countDownLatch.countDown();


        }
    }
}
