package leetcode.editor.cn.myLock;

import java.util.concurrent.locks.ReentrantLock;

public class TestReentrantLock {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        for (int i = 0; i < 10 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + " start");
                lock.lock();
                System.out.println(Thread.currentThread().getName() + " runing");
                lock.lock();
                System.out.println(Thread.currentThread().getName() + " runing2");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " end");
                lock.unlock();
                lock.unlock();
            }).start();
        }
    }
}
