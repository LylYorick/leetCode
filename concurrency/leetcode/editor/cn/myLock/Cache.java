package leetcode.editor.cn.myLock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 多线程 读写锁 示例 缓存实现
 */
public class Cache {
    static Map<String, Object> map = new HashMap<String, Object>();
    static ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    static Lock r = rwl.readLock();
    static Lock w = rwl.writeLock();
    // 获取一个key对应的value
    public static final Object get(String key) {
        r.lock();
        try {
            return map.get(key);
        } finally {
            r.unlock();
        }
    }// 设置key对应的value，并返回旧的value
    public static final Object put(String key, Object value) {
        w.lock();
        try {
            return map.put(key, value);
        } finally {
            w.unlock();
        }
    }// 清空所有的内容
    public static final void clear() {
        w.lock();
        try {
            map.clear();
        } finally {
            w.unlock();
        }
    }

    public static void main(String[] args) {
        int s = 0x0002FFFF;
         s = s >>> 16;
        System.out.println(Integer.toBinaryString(s));
        int SHARED_SHIFT = 16;

        int i1 = 1 << SHARED_SHIFT;

//        System.out.println(Integer.toBinaryString(i1 ) + " " +  i1+ " " );
        System.out.println(Integer.toBinaryString(i1 +1) + " " + (65536+1));
        int i = i1 - 1;
        System.out.println(Integer.toBinaryString(i));
//        根据状态的划分能得出一个推论：S不等于0时，当写状态（S&0x0000FFFF）等于0时，则读
//        状态（S>>>16）大于0，即读锁已被获取
    }
}