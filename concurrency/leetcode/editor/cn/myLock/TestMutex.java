package leetcode.editor.cn.myLock;

public class TestMutex {
    public static void main(String[] args) {
        Mutex  lock = new Mutex();
        for (int i = 0; i < 10 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + " start");
                lock.lock();
                System.out.println(Thread.currentThread().getName() + " runing");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " end");
                lock.unlock();
            }).start();
        }
    }
}
