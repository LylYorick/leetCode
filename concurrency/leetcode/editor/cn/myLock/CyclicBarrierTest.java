package leetcode.editor.cn.myLock;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierTest {
   static CyclicBarrier c = new CyclicBarrier(6,()->{
       System.out.println("集齐七龙珠 召唤神龙");
   });
    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            final int temp = i;
            new Thread(()->{
                System.out.println("收集到第" + temp + "颗龙珠！");
                if(temp ==2){
                    c.reset();
                }
                try {
                    c.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();

                } catch (BrokenBarrierException e) {
                    System.out.println(temp +"出现InterruptedException异常");
                    e.printStackTrace();
                }


            }).start();
        }
    }

}
