package leetcode.editor.cn.concurrent;

import java.util.concurrent.*;

public class MyExecutor {
    private  ExecutorService  executor = new ThreadPoolExecutor(2,3,10L, TimeUnit.SECONDS,new LinkedBlockingDeque<>(1), Executors.defaultThreadFactory(),new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        Executors.newCachedThreadPool();
    }

    public void test(){
        executor.submit(()->{
            try {
                setA();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.submit(()->{
            try {
                setB("直接进setB");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.submit(()->{
            try {
                setB("直接进setB2");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.submit(()->{
            try {
                setB("直接进setB3");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.submit(()->{
            try {
                setB("直接进setB4");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.submit(()->{
            try {
                setB("直接进setB5");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public  void setA() throws InterruptedException {
        System.out.println("in setA");
        Thread.sleep(5000);
        setB("set A to setB");
    }


    public  void setB(String str) throws InterruptedException {

        System.out.println(str);
        Thread.sleep(2000);
    }
}
