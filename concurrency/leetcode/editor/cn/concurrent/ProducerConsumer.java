package leetcode.editor.cn.concurrent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 使用阻塞队列实现
 */
public class ProducerConsumer {
    public static void main(String[] args) {
        Resource resource = new Resource();
        Producer p1 = new Producer(resource);
//        Producer p2 = new Producer(resource);
        Consumer c1 = new Consumer(resource);
      p1.start();
//        p2.start();
        c1.start();
    }
}
class Resource{
    BlockingQueue queue = new LinkedBlockingQueue(10);
    //添加资源
    public void add() {
        try {
            queue.put(1);
            System.out.println("生产者"+Thread.currentThread().getName()+":"+queue.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //消费资源
    public void remove(){
        try {
            queue.take();
            System.out.println("消费者"+Thread.currentThread().getName()+":"+queue.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
class Producer extends Thread{
    private Resource resource;
    public Producer(Resource resource){
        this.resource = resource;
    }
    public void run(){
        while (true){
            try {
                Thread.sleep((long) (1000+Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            resource.add();
        }
    }
}
class Consumer extends Thread{
    private Resource resource;
    public Consumer(Resource resource){
        this.resource = resource;
    }
    public void run(){
        while (true){
            try {
                Thread.sleep((long) (1000+Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            resource.remove();
        }
    }
}