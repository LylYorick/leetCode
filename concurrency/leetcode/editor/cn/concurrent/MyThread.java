package leetcode.editor.cn.concurrent;

public class MyThread  extends Thread{
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        //判断线程是否存活
        System.out.println("是否存活:" + myThread.isAlive());
        //判断线程是否中断
        System.out.println("是否中断:" + myThread.isInterrupted());
//        //中断 这个子线程线程
//        myThread.interrupt();
        //判断线程是否中断
        System.out.println("是否中断:" + myThread.isInterrupted());
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        //判断是否存活
        System.out.println("是否存活" + myThread.isAlive());
    }

    @Override
    public void run() {
        super.run();
        this.interrupt();

        for (int i = 0; i < 100; i++) {
            try {
                //休息当前线程5秒
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("正在执行" + i);
        }
    }
}
