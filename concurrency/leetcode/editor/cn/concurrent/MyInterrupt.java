package leetcode.editor.cn.concurrent;

public class MyInterrupt extends Thread{

    @Override
    public void run(){
        try{
            boolean flag = true;
            while(!Thread.currentThread().isInterrupted() ){
                // 做事情
                Thread.sleep(500);
            }
        }catch(InterruptedException e){
            //在sleep 和wait时 调用 interrupte，会报异常
        }
        finally{
            // cleanup, if required
        }
    }
}
