package leetcode.editor.cn.base;

//继承方式创建多线程
public class HowBuildConcurrency2 extends  Thread{
    public  static  int count = 10000;
    @Override
    public void run() {
        count --;
       // System.out.println( "继承thread 创建线程" +Thread.currentThread().getId() + " " + Thread.currentThread().getName());
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10000 ; i++) {
            HowBuildConcurrency2 howBuildConcurrency2 = new HowBuildConcurrency2();
            howBuildConcurrency2.start();
        }
        Thread.sleep(2000);
        System.out.println(count);
    }
}
