package leetcode.editor.cn.base;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MyStream {
    public static void main(String[] args) {
        List<String> notifyEmployeeIdList = Arrays.asList("".split(","));
        System.out.println(notifyEmployeeIdList.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList()).size());
    }
}
