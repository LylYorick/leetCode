package leetcode.editor.cn.base;
//使用实现 runable接口方式 和 匿名内部类实现多线程
public class HowBuildConcurrency implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep((int)(Math.random() * 10));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getId() + " " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
//        for (int i = 0; i < 10 ; i++) {
//            Thread thread = new Thread(new HowBuildConcurrency());
//            thread.start();
//        }

        for (int i = 0; i < 10 ; i++) {
           new Thread(){
                public void run() {
                    System.out.println( "匿名内部类 创建多线程" +Thread.currentThread().getId() + " " + Thread.currentThread().getName());
                }
            }.start();
        }

//        thread = new Thread(new HowBuildConcurrency());
//        thread.run();
    }


}
