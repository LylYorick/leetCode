//题目编号: 1115 题目名称: 交替打印FooBar
//我们提供一个类： 
//
// 
//class FooBar {
//  public void foo() {
//    for (int i = 0; i < n; i++) {
//      print("foo");
//    }
//  }
//
//  public void bar() {
//    for (int i = 0; i < n; i++) {
//      print("bar");
//    }
//  }
//}
// 
//
// 两个不同的线程将会共用一个 FooBar 实例。其中一个线程将会调用 foo() 方法，另一个线程将会调用 bar() 方法。 
//
// 请设计修改程序，以确保 "foobar" 被输出 n 次。 
//
// 
//
// 示例 1: 
//
// 
//输入: n = 1
//输出: "foobar"
//解释: 这里有两个线程被异步启动。其中一个调用 foo() 方法, 另一个调用 bar() 方法，"foobar" 将被输出一次。
// 
//
// 示例 2: 
//
// 
//输入: n = 2
//输出: "foobarfoobar"
//解释: "foobar" 将被输出两次。
// 
//

package leetcode.editor.cn;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PrintFoobarAlternately1115 {
    public static void main(String[] args) throws InterruptedException {
        PrintFoobarAlternately111502.FooBar fooBar = new PrintFoobarAlternately111502().new FooBar(5);
        Thread t1 = new Thread(()->{
            try {
                fooBar.foo(()->{
                    System.out.print("foo");
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() ->{
            try {
                fooBar.bar(()->{
                    System.out.println("bar");
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        //Solution solution = new PrintFoobarAlternately1115().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class FooBar {
    private int n;
    Lock lock = new ReentrantLock();
    volatile boolean flag = true;
    public FooBar(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        
        for (int i = 0; i < n;) {
            lock.lock();
            try {
                if(flag){
                    // printFoo.run() outputs "foo". Do not change or remove this line.
                    printFoo.run();
                    flag = false;
                    i++;
                }
            }finally {
               lock.unlock();
            }
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        
        for (int i = 0; i < n;) {
            lock.lock();
            try {
                if(!flag){
                   // printBar.run() outputs "bar". Do not change or remove this line.
                    printBar.run();
                    flag = true;
                    i++;
                }
            }finally {
                lock.unlock();
            }

        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}