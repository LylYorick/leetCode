package leetcode.editor.cn.juc;

import java.util.concurrent.Semaphore;

public class SemaphoreTest {
    private  Semaphore t1 = new Semaphore(1);
    private Semaphore t2 = new Semaphore(0);
    private void print1(){
        try {
            t1.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程1打印");
        t2.release();
    }

    private void print2(){
        try {
            t2.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程2打印");
        t1.release();

    }

    public static void main(String[] args) {
        SemaphoreTest semaphoreTest = new SemaphoreTest();
        new Thread(()->{
            for (int i = 0; i < 2 ; i++) {
                semaphoreTest.print1();
            }
        }).start();

        new Thread(()->{
            for (int i = 0; i < 2 ; i++) {
                semaphoreTest.print2();
            }
        }).start();


    }
}
