package leetcode.editor.cn;

//题目编号: 1116 题目名称: 打印零与奇偶数

//假设有这么一个类： 
//
// class ZeroEvenOdd {
//  public ZeroEvenOdd(int n) { ... }      // 构造函数
//  public void zero(printNumber) { ... }  // 仅打印出 0
//  public void even(printNumber) { ... }  // 仅打印出 偶数
//  public void odd(printNumber) { ... }   // 仅打印出 奇数
//}
// 
//
// 相同的一个 ZeroEvenOdd 类实例将会传递给三个不同的线程： 
//
// 
// 线程 A 将调用 zero()，它只输出 0 。 
// 线程 B 将调用 even()，它只输出偶数。 
// 线程 C 将调用 odd()，它只输出奇数。 
// 
//
// 每个线程都有一个 printNumber 方法来输出一个整数。请修改给出的代码以输出整数序列 010203040506... ，其中序列的长度必须为 2n
//。 
//
// 
//
// 示例 1： 
//
// 输入：n = 2
//输出："0102"
//说明：三条线程异步执行，其中一个调用 zero()，另一个线程调用 even()，最后一个线程调用odd()。正确的输出为 "0102"。
// 
//
// 示例 2： 
//
// 输入：n = 5
//输出："0102030405"
// 
// 👍 66 👎 0

import java.util.concurrent.Semaphore;
import java.util.function.IntConsumer;

public class PrintZeroEvenOdd111601 {
	public static void main(String[] args) throws InterruptedException {
        ZeroEvenOdd zeroEvenOdd = new PrintZeroEvenOdd111601().new ZeroEvenOdd(1);
        Thread thread1 = new Thread(()->{
            try {
                zeroEvenOdd.zero(value -> System.out.print(value));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread thread2 = new Thread(()->{
            try {
                zeroEvenOdd.odd(value -> System.out.print(value));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread thread3 = new Thread(()->{
            try {
                zeroEvenOdd.even(value -> System.out.print(value));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();

    }
//leetcode submit region begin(Prohibit modification and deletion)
class ZeroEvenOdd {
    private int n;
    private  volatile int i = 1;
    private Semaphore zeroSemaphore = new Semaphore(1);
    private Semaphore evenSemaphore = new Semaphore(0);
    private Semaphore oddSemaphore = new Semaphore(0);
    public ZeroEvenOdd(int n) {
        this.n = n;
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void zero(IntConsumer printNumber) throws InterruptedException {
        for (; i <= n;) {
            zeroSemaphore.acquire();
            if(i<= n){
                printNumber.accept(0);
            }
            if (i % 2 == 0){
                evenSemaphore.release();
            }else{
                oddSemaphore.release();
            }

        }

    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        for (; i <=n;) {
            evenSemaphore.acquire();
            if(i<=n){
            printNumber.accept(i);
            i++;
            }
            zeroSemaphore.release();
        }

    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        for (; i <= n;) {
            oddSemaphore.acquire();
            if(i<=n){
                printNumber.accept(i);
                i++;
            }
            zeroSemaphore.release();

        }

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
