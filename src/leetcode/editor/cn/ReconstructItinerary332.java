package leetcode.editor.cn;

//题目编号: 332 题目名称: 重新安排行程

//给定一个机票的字符串二维数组 [from, to]，子数组中的两个成员分别表示飞机出发和降落的机场地点，对该行程进行重新规划排序。所有这些机票都属于一个从 
//JFK（肯尼迪国际机场）出发的先生，所以该行程必须从 JFK 开始。 
//
// 说明: 
//
// 
// 如果存在多种有效的行程，你可以按字符自然排序返回最小的行程组合。例如，行程 ["JFK", "LGA"] 与 ["JFK", "LGB"] 相比就更小，排
//序更靠前 
// 所有的机场都用三个大写字母表示（机场代码）。 
// 假定所有机票至少存在一种合理的行程。 
// 
//
// 示例 1: 
//
// 输入: [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
//输出: ["JFK", "MUC", "LHR", "SFO", "SJC"]
// [JFK, MUC, LHR, SFO, SJC]
// 
//
// 示例 2: 
//
// 输入: [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
//输出: ["JFK","ATL","JFK","SFO","ATL","SFO"]
//解释: 另一种有效的行程是 ["JFK","SFO","ATL","JFK","ATL","SFO"]。但是它自然排序更大更靠后。 
// Related Topics 深度优先搜索 图 
// 👍 227 👎 0


import java.util.*;

public class ReconstructItinerary332{
	public static void main(String[] args) {
		//String test ="[[\"EZE\",\"TIA\"],[\"EZE\",\"AXA\"],[\"AUA\",\"EZE\"],[\"EZE\",\"JFK\"],[\"JFK\",\"ANU\"],[\"JFK\",\"ANU\"],[\"AXA\",\"TIA\"],[\"JFK\",\"AUA\"],[\"TIA\",\"JFK\"],[\"ANU\",\"EZE\"],[\"ANU\",\"EZE\"],[\"TIA\",\"AUA\"]]";
//		String test ="[[\"AXA\",\"EZE\"],[\"EZE\",\"AUA\"],[\"ADL\",\"JFK\"]," +
//				"[\"ADL\",\"TIA\"],[\"AUA\",\"AXA\"],[\"EZE\",\"TIA\"],[\"EZE\",\"TIA\"]," +
//				"[\"AXA\",\"EZE\"],[\"EZE\",\"ADL\"],[\"ANU\",\"EZE\"],[\"TIA\",\"EZE\"],[\"JFK\",\"ADL\"]," +
//				"[\"AUA\",\"JFK\"],[\"JFK\",\"EZE\"],[\"EZE\",\"ANU\"],[\"ADL\",\"AUA\"],[\"ANU\",\"AXA\"]," +
//				"[\"AXA\",\"ADL\"],[\"AUA\",\"JFK\"],[\"EZE\",\"ADL\"],[\"ANU\",\"TIA\"],[\"AUA\",\"JFK\"]," +
//				"[\"TIA\",\"JFK\"],[\"EZE\",\"AUA\"],[\"AXA\",\"EZE\"],[\"AUA\",\"ANU\"],[\"ADL\",\"AXA\"]," +
//				"[\"EZE\",\"ADL\"],[\"AUA\",\"ANU\"],[\"AXA\",\"EZE\"],[\"TIA\",\"AUA\"],[\"AXA\",\"EZE\"]," +
//				"[\"AUA\",\"SYD\"],[\"ADL\",\"JFK\"],[\"EZE\",\"AUA\"],[\"ADL\",\"ANU\"],[\"AUA\",\"TIA\"]," +
//				"[\"ADL\",\"EZE\"],[\"TIA\",\"JFK\"],[\"AXA\",\"ANU\"],[\"JFK\",\"AXA\"],[\"JFK\",\"ADL\"]," +
//				"[\"ADL\",\"EZE\"],[\"AXA\",\"TIA\"],[\"JFK\",\"AUA\"],[\"ADL\",\"EZE\"],[\"JFK\",\"ADL\"]," +
//				"[\"ADL\",\"AXA\"],[\"TIA\",\"AUA\"],[\"AXA\",\"JFK\"],[\"ADL\",\"AUA\"],[\"TIA\",\"JFK\"],[\"JFK\",\"ADL\"]," +
//				"[\"JFK\",\"ADL\"],[\"ANU\",\"AXA\"],[\"TIA\",\"AXA\"],[\"EZE\",\"JFK\"],[\"EZE\",\"AXA\"],[\"ADL\",\"TIA\"],[\"JFK\",\"AUA\"],[\"TIA\",\"EZE\"]," +
//				"[\"EZE\",\"ADL\"],[\"JFK\",\"ANU\"],[\"TIA\",\"AUA\"],[\"EZE\",\"ADL\"],[\"ADL\",\"JFK\"],[\"ANU\",\"AXA\"],[\"AUA\",\"AXA\"],[\"ANU\",\"EZE\"]," +
//				"[\"ADL\",\"AXA\"],[\"ANU\",\"AXA\"],[\"TIA\",\"ADL\"],[\"JFK\",\"ADL\"],[\"JFK\",\"TIA\"],[\"AUA\",\"ADL\"],[\"AUA\",\"TIA\"],[\"TIA\",\"JFK\"]," +
//				"[\"EZE\",\"JFK\"],[\"AUA\",\"ADL\"],[\"ADL\",\"AUA\"],[\"EZE\",\"ANU\"],[\"ADL\",\"ANU\"],[\"AUA\",\"AXA\"],[\"AXA\",\"TIA\"],[\"AXA\",\"TIA\"]," +
//				"[\"ADL\",\"AXA\"],[\"EZE\",\"AXA\"],[\"AXA\",\"JFK\"],[\"JFK\",\"AUA\"],[\"ANU\",\"ADL\"],[\"AXA\",\"TIA\"],[\"ANU\",\"AUA\"],[\"JFK\",\"EZE\"]," +
//				"[\"AXA\",\"ADL\"],[\"TIA\",\"EZE\"],[\"JFK\",\"AXA\"],[\"AXA\",\"ADL\"],[\"EZE\",\"AUA\"],[\"AXA\",\"ANU\"],[\"ADL\",\"EZE\"],[\"AUA\",\"EZE\"]]";

		String test = "[[\"MUC\", \"LHR\"], [\"JFK\", \"MUC\"], [\"SFO\", \"SJC\"], [\"LHR\", \"SFO\"]]";
		//String test = "[[\"JFK\",\"SFO\"],[\"JFK\",\"ATL\"],[\"SFO\",\"ATL\"],[\"ATL\",\"JFK\"],[\"ATL\",\"SFO\"]]";
		Solution solution = new ReconstructItinerary332().new Solution();
		List<List> lists = null;
		List<List<String>> lists2 = new ArrayList<>();
		for (List item : lists) {
			lists2.add(item);
		}
		System.out.println(solution.dfs(lists2,"JFK"));
		//System.out.println(solution.findItinerary(lists2));
		//["JFK","ATL","JFK","SFO","ATL","SFO"]

//		List<String> item = Stream.of("MUC", "LHR").collect(Collectors.toList());
//		List<String> item1 = Stream.of("JFK", "MUC").collect(Collectors.toList());
//		List<String> item2 = Stream.of("SFO", "SJC").collect(Collectors.toList());
//		List<String> item3 = Stream.of("LHR", "SFO").collect(Collectors.toList());
//		List<List<String>> tickets = Stream.of(item,item1,item2,item3).collect(Collectors.toList());
//		//["JFK", "MUC", "LHR", "SFO", "SJC"]
//		System.out.println(solution.findItinerary(tickets));
//		List<String> item = Stream.of("JFK","KUL").collect(Collectors.toList());
//		List<String> item1 = Stream.of("JFK","NRT").collect(Collectors.toList());
//		List<String>item2 = Stream.of("NRT","JFK").collect(Collectors.toList());
//		List<List<String>> tickets = Stream.of(item,item1,item2).collect(Collectors.toList());
//		System.out.println(solution.findItinerary(tickets));
	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
	//private	List<String> res = new ArrayList<>();

//    public List<String> findItinerary(List<List<String>> tickets) {
//		res = Stream.of("JFK").collect(Collectors.toList());
//		dfs(tickets,"JFK",res);
//		return  res;
//    }
    public List<String>  dfs(List<List<String>> tickets,String top){
//		Map<String, List<String>> graph = new HashMap<>();
//		for (List<String> pair : tickets) {
//			// 因为涉及删除操作，我们用链表
//			List<String> nbr = graph.computeIfAbsent(pair.get(0), k -> new LinkedList<>());
//			nbr.add(pair.get(1));
//		}
//		// 按目的顶点排序
//		graph.values().forEach(x -> x.sort(String::compareTo));
		Collections.sort(tickets, new Comparator<List<String>>() {
			@Override
			public int compare(List<String> o1, List<String> o2) {
				return o1.get(1).compareTo(o2.get(1));
			}
		});
		System.out.println(tickets);

		Stack<List<String>> stack = new Stack<>();
		for (int i = 0; i < tickets.size(); i++) {
			List<String> ticket= tickets.get(i);
			if(top.equals(ticket.get(0))){
				stack.push(ticket);
			}
		}
		int size = tickets.size();
		while(stack.size() != size){
			List<String> peek = stack.peek();
			 top = peek.get(1);
			List<List<String>> temp = new LinkedList<>();
			for (int i = 0; i < tickets.size(); i++) {
				List<String> ticket= tickets.get(i);
				if(top.equals(ticket.get(0))){
					temp.add(ticket);
				}
			}
			if(temp.isEmpty()){
				List<String> pop = stack.pop();
				tickets.add(pop);
			}else{
				List<String> item = temp.get(0);
				stack.push(item);
				tickets.remove(item);
			}
		};
		List<String> list = new ArrayList<>();
		list.add("JFK");
		for (int i = 0; i < stack.size(); i++) {
			list.add(stack.get(i).get(1));
		}
		return list;

	}
 /*   public boolean dfs(List<List<String>> tickets,String top,List<String> res){
    	if(tickets.isEmpty()){
    		return true;
		}
   		boolean flag = false;
		List<List<String>> temp = new LinkedList<>();
		for (int i = 0; i < tickets.size(); i++) {
			List<String> ticket= tickets.get(i);
			if(top.equals(ticket.get(0))){
				temp.add(ticket);
				flag = true;
			}
		}
		//没找到
		if(flag == false){
			return  false;
		}

		String minstr = "[";
		List minList = null;
		for (List<String> ticket : temp) {
			tickets.remove(ticket);
			List list  = new ArrayList<>(res);
			list.add(ticket.get(1));
			flag = dfs(tickets,ticket.get(1),list);
			tickets.add(ticket);
			if(flag && minstr.compareTo(ticket.get(1)) >0){
				minstr  = ticket.get(1);
				minList = list;
			}
		}
		if (minList == null){
			return  false;
		}
		res.clear();
		res.addAll(minList);
		return true;
	}*/
}
//leetcode submit region end(Prohibit modification and deletion)

}
