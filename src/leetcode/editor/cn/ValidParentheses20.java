package leetcode.editor.cn;

//题目编号: 20 题目名称: 有效的括号

//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。 
//
// 有效字符串需满足： 
//
// 
// 左括号必须用相同类型的右括号闭合。 
// 左括号必须以正确的顺序闭合。 
// 
//
// 注意空字符串可被认为是有效字符串。 
//
// 示例 1: 
//
// 输入: "()"
//输出: true
// 
//
// 示例 2: 
//
// 输入: "()[]{}"
//输出: true
// 
//
// 示例 3: 
//
// 输入: "(]"
//输出: false
// 
//
// 示例 4: 
//
// 输入: "([)]"
//输出: false
// 
//
// 示例 5: 
//
// 输入: "{[]}"
//输出: true 
// Related Topics 栈 字符串 
// 👍 1761 👎 0

import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValidParentheses20{
	public static void main(String[] args) {
		 Solution solution = new ValidParentheses20().new Solution();
//		System.out.println((int)'(');
//		System.out.println((int)')');
//		System.out.println((int)'{');
//		System.out.println((int)'}');
//		System.out.println((int)'[');
//		System.out.println((int)']');
		System.out.println(solution.isValid("{[]}"));
		System.out.println(solution.isValid("{"));
//		System.out.println(solution.isValid("()[]{}"));
//		System.out.println(solution.isValid("(}"));
	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean isValid(String s) {
		if(s.isEmpty()){
			return true;
		}
		Stack<Character> stack = new Stack<>();
		List<Character> list = Stream.of('(','[','{').collect(Collectors.toList());
		for (int i = 0; i < s.length(); i++) {
			if( list.contains(s.charAt(i))){
				stack.push(s.charAt(i));
			}else{
				if(stack.isEmpty()){
					return false;
				}
				Character peek = stack.peek();
				if (peek.equals('(') && '('+1 == s.charAt(i)){
					stack.pop();
					continue;
				}else if((peek.equals('{') || peek.equals('[')) &&  peek +2 == s.charAt(i)){
					stack.pop();
				}else{
					return false;
				}
			}
		}
		if(!stack.isEmpty()){
			return  false;
		}
		return true;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
