package leetcode.editor.cn;

//题目编号: 111 题目名称: 二叉树的最小深度

//给定一个二叉树，找出其最小深度。 
//
// 最小深度是从根节点到最近叶子节点的最短路径上的节点数量。 
//
// 说明: 叶子节点是指没有子节点的节点。 
//
// 示例: 
//
// 给定二叉树 [3,9,20,null,null,15,7], 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
//
// 返回它的最小深度 2. 
// Related Topics 树 深度优先搜索 广度优先搜索 
// 👍 336 👎 0

import com.sun.jmx.remote.internal.ArrayQueue;
import sun.reflect.generics.tree.Tree;

import java.util.*;

public class MinimumDepthOfBinaryTree111{
	public static void main(String[] args) {
		Solution solution = new MinimumDepthOfBinaryTree111().new Solution();
		TreeNode treeNode = new MinimumDepthOfBinaryTree111().new TreeNode(3, new MinimumDepthOfBinaryTree111().new TreeNode(9),
				new MinimumDepthOfBinaryTree111().new TreeNode(20, new MinimumDepthOfBinaryTree111().new TreeNode(15),
						new MinimumDepthOfBinaryTree111().new TreeNode(7)));
		System.out.println(solution.minDepth(treeNode));

		TreeNode treeNode2= new MinimumDepthOfBinaryTree111().new TreeNode(3, new MinimumDepthOfBinaryTree111().new TreeNode(9), null);
		System.out.println(solution.minDepth(treeNode2));

		TreeNode treeNode3 = new MinimumDepthOfBinaryTree111().new TreeNode(1, new MinimumDepthOfBinaryTree111().new TreeNode(2,
				new MinimumDepthOfBinaryTree111().new TreeNode(4),null),
				new MinimumDepthOfBinaryTree111().new TreeNode(3,null,
						new MinimumDepthOfBinaryTree111().new TreeNode(5)));
		System.out.println(solution.minDepth(treeNode3));


	}
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) { this.val = val; }
	TreeNode(int val,  TreeNode left,  TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
class Solution {
    public int minDepth(TreeNode root) {
    	if(root == null){
    		return  0;
		}

    	Queue<List<TreeNode>> queue = new ArrayDeque<>();
		List<TreeNode> list = new ArrayList<>();
		list.add(root);
		queue.add(list);
		int minDepth = 1;
		while(!queue.isEmpty()){
			List<TreeNode> topList = queue.peek();
			List<TreeNode> lastList = new ArrayList<>();
			for (TreeNode treeNode : topList) {
				if(treeNode.left == null && treeNode.right == null){
					return  minDepth;
				}
				if(treeNode.left != null) lastList.add(treeNode.left);
				if(treeNode.right != null) lastList.add(treeNode.right);
			}
			queue.add(lastList);
			queue.remove();
			minDepth++;
		}
		return minDepth;

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
