package leetcode.editor.cn;

//题目编号: 491 题目名称: 递增子序列

//给定一个整型数组, 你的任务是找到所有该数组的递增子序列，递增子序列的长度至少是2。 
//
// 示例: 
//
// 
//输入: [4, 6, 7, 7]
//输出: [[4, 6], [4, 7], [4, 6, 7], [4, 6, 7, 7], [6, 7], [6, 7, 7], [7,7], [4,7,7
//]] 
//
// 说明: 
//
// 
// 给定数组的长度不会超过15。 
// 数组中的整数范围是 [-100,100]。 
// 给定数组中可能包含重复数字，相等的数字应该被视为递增的一种情况。 
// 
// Related Topics 深度优先搜索 
// 👍 118 👎 0


import java.util.*;


public class IncreasingSubsequences491{
	public static void main(String[] args) {
		Solution solution = new IncreasingSubsequences491().new Solution();
		List<List<Integer>> subsequences = solution.findSubsequences(new int[]{ 7,6,7, 7});
		System.out.println(subsequences);

	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {

	// 定义全局变量保存结果
	List<List<Integer>> res = new ArrayList<>();

	public List<List<Integer>> findSubsequences(int[] nums) {
		// idx 初始化为 -1，开始 dfs 搜索。
		dfs(nums, -1, new ArrayList<>());
		return res;
	}

	private void dfs(int[] nums, int idx, List<Integer> curList) {
		// 只要当前的递增序列长度大于 1，就加入到结果 res 中，然后继续搜索递增序列的下一个值。
		if (curList.size() > 1) {
			res.add(new ArrayList<>(curList));
		}

	}


}
//leetcode submit region end(Prohibit modification and deletion)

}
