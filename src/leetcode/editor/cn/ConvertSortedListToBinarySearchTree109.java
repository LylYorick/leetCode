package leetcode.editor.cn;

//题目编号: 109 题目名称: 有序链表转换二叉搜索树

//给定一个单链表，其中的元素按升序排序，将其转换为高度平衡的二叉搜索树。 
//
// 本题中，一个高度平衡二叉树是指一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1。 
//
// 示例: 
//
// 给定的有序链表： [-10, -3, 0, 5, 9],
//
//一个可能的答案是：[0, -3, 9, -10, null, 5], 它可以表示下面这个高度平衡二叉搜索树：
//
//      0
//     / \
//   -3   9
//   /   /
// -10  5
// 
// Related Topics 深度优先搜索 链表 
// 👍 347 👎 0

import java.util.LinkedList;
import java.util.List;

public class ConvertSortedListToBinarySearchTree109{
	public static void main(String[] args) {
		Solution solution = new ConvertSortedListToBinarySearchTree109().new Solution();
		
	}
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
// Definition for singly-linked list.
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) { this.val = val; }
	ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}

	//Definition for a binary tree node.
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode() {}
		TreeNode(int val) { this.val = val; }
		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}
class Solution {
	public  TreeNode sortedListToBST( ListNode head) {
		List<Integer> list = new LinkedList<>();
		while(head!=null){
			list.add(head.val);
			head = head.next;
		}
		if(list.isEmpty()){
			return null;
		}
		if(list.size()  == 1){
			return new  TreeNode(list.get(0));
		}
		int middle = list.size()/2;
		 TreeNode root = new  TreeNode(list.get(middle));
		root.left = getLeftNode(list,middle);
		root.right = getRighttNode(list,middle,list.size());
		return  root;
	}
	public  TreeNode getLeftNode(List<Integer> list, int end){
		if(1 == end){
			return  new  TreeNode(list.get(0));
		};

		int middle = end /2;


		 TreeNode item = new  TreeNode(list.get(middle));
		item.left = getLeftNode(list,middle);
		item.right = getRighttNode(list,middle,end);
		return  item;
	}

	public  TreeNode getRighttNode(List <Integer> list, int start, int end){
		if(start +1 == end){
			return null;
		}
		int middle =  (start + end) /2;
		 TreeNode item = new  TreeNode(list.get(middle));
		item.left = getRighttNode(list,start,middle);
		item.right = getRighttNode(list,middle,end);
		return  item;
	}
}
//leetcode submit region end(Prohibit modification and deletion)

}
