package leetcode.editor.cn;

//题目编号: 343 题目名称: 整数拆分

//给定一个正整数 n，将其拆分为至少两个正整数的和，并使这些整数的乘积最大化。 返回你可以获得的最大乘积。 
//
// 示例 1: 
//
// 输入: 2
//输出: 1
//解释: 2 = 1 + 1, 1 × 1 = 1。 
//
// 示例 2: 
//
// 输入: 10
//输出: 36
//解释: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36。 
//
// 说明: 你可以假设 n 不小于 2 且不大于 58。 
// Related Topics 数学 动态规划 
// 👍 290 👎 0

public class IntegerBreak{
	public static void main(String[] args) {
		Solution solution = new IntegerBreak().new Solution();
		System.out.println(solution.integerBreak(3));
	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int integerBreak(int n) {
    	int[] dp = new int[n+1];
		dp[0] = 0;
		dp[1] = 0;
		dp[2] = 1;
		if (n == 2) return  dp[n];
		for (int i = 2; i <= n ; i++) {
			int max = 0;
			for (int j = 2; j < i ; j++) {
				int temp = j * Math.max(i - j, dp[i - j]);
				max = Math.max(temp,max);
			}
			dp[i] = max;
		}
		return  dp[n];
    }
//    public int calc(int n){
//    	if ( n == 2){
//    		return  1;
//		}
//    	Math.
//	}
}
//leetcode submit region end(Prohibit modification and deletion)

}
