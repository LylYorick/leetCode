package leetcode.editor.cn;



import java.math.BigDecimal;
import java.util.Scanner;
// 牛客 求立方根
public class LifangGen {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String next = scanner.next();
            double input = Double.parseDouble(next);
           double result = 0;
            for (int i = 0; i < input; i++) {
                if(i*i*i > input) break;
                result = i;
            }
            if(result * result * result == input){
                System.out.println(String.format("%.1f", result));
            }
            BigDecimal bigger = new BigDecimal(result + 1);
            BigDecimal inputBig = new BigDecimal(input);
            for (BigDecimal i = new BigDecimal(result); i.compareTo(bigger) < 0; i = i.add(new BigDecimal("0.01"))){
                System.out.println(i.multiply(i).multiply(i));
                if(i.multiply(i).multiply(i).compareTo(inputBig) > 0 ) break;
                result = i.doubleValue();
            }
            System.out.println(String.format("%.1f", result));

        }
    }
}
