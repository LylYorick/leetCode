package leetcode.editor.cn;

//题目编号: 529 题目名称: 扫雷游戏

//让我们一起来玩扫雷游戏！ 
//
// 给定一个代表游戏板的二维字符矩阵。 'M' 代表一个未挖出的地雷，'E' 代表一个未挖出的空方块，'B' 代表没有相邻（上，下，左，右，和所有4个对角线）
//地雷的已挖出的空白方块，数字（'1' 到 '8'）表示有多少地雷与这块已挖出的方块相邻，'X' 则表示一个已挖出的地雷。 
//
// 现在给出在所有未挖出的方块中（'M'或者'E'）的下一个点击位置（行和列索引），根据以下规则，返回相应位置被点击后对应的面板： 
//
// 
// 如果一个地雷（'M'）被挖出，游戏就结束了- 把它改为 'X'。 
// 如果一个没有相邻地雷的空方块（'E'）被挖出，修改它为（'B'），并且所有和其相邻的未挖出方块都应该被递归地揭露。 
// 如果一个至少与一个地雷相邻的空方块（'E'）被挖出，修改它为数字（'1'到'8'），表示相邻地雷的数量。 
// 如果在此次点击中，若无更多方块可被揭露，则返回面板。 
// 
//
// 
//
// 示例 1： 
//
// 输入: 
//
//[['E', 'E', 'E', 'E', 'E'],
// ['E', 'E', 'M', 'E', 'E'],
// ['E', 'E', 'E', 'E', 'E'],
// ['E', 'E', 'E', 'E', 'E']]
//
//Click : [3,0]
//
//输出: 
//
//[['B', '1', 'E', '1', 'B'],
// ['B', '1', 'M', '1', 'B'],
// ['B', '1', '1', '1', 'B'],
// ['B', 'B', 'B', 'B', 'B']]
//
//解释:
//
// 
//
// 示例 2： 
//
// 输入: 
//
//[['B', '1', 'E', '1', 'B'],
// ['B', '1', 'M', '1', 'B'],
// ['B', '1', '1', '1', 'B'],
// ['B', 'B', 'B', 'B', 'B']]
//
//Click : [1,2]
//
//输出: 
//
//[['B', '1', 'E', '1', 'B'],
// ['B', '1', 'X', '1', 'B'],
// ['B', '1', '1', '1', 'B'],
// ['B', 'B', 'B', 'B', 'B']]
//
//解释:
//
// 
//
// 
//
// 注意： 
//
// 
// 输入矩阵的宽和高的范围为 [1,50]。 
// 点击的位置只能是未被挖出的方块 ('M' 或者 'E')，这也意味着面板至少包含一个可点击的方块。 
// 输入面板不会是游戏结束的状态（即有地雷已被挖出）。 
// 简单起见，未提及的规则在这个问题中可被忽略。例如，当游戏结束时你不需要挖出所有地雷，考虑所有你可能赢得游戏或标记方块的情况。 
// 
// Related Topics 深度优先搜索 广度优先搜索 
// 👍 157 👎 0

import java.util.*;

public class Minesweeper529{
	public static void main(String[] args) {
		Solution solution = new Minesweeper529().new Solution();
		List<int[]> queue = new ArrayList<>();
		queue.add(new int[]{1,2});
		System.out.println(new int[]{1,2}.equals(new int[]{1,2}));
		System.out.println(Arrays.equals(queue.get(0),new int[]{1,2}));
		char[][] board = {{'E', 'E', 'E', 'E', 'E'},
						  {'E', 'E', 'M', 'E', 'E'},
						  {'E', 'E', 'E', 'E', 'E'},
				          {'E', 'E', 'E', 'E', 'E'}};
		int[] click = new int[]{3,0};
		char[][] chars = solution.updateBoard(board, click);
		for (char[] aChar : chars) {
			System.out.println(Arrays.toString(aChar));
		}

	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
	int[] dx = {-1, 1, 0, 0, -1, 1, -1, 1};
	int[] dy = {0, 0, -1, 1, -1, 1, 1, -1};
    public char[][] updateBoard(char[][] board, int[] click) {
		int maxY = board.length;
		int maxX = board[0].length;
		List<int[]> queue = new ArrayList<>();
		queue.add(click);
		for (int county = 0; county < queue.size() ; county++) {
			int[] item = queue.get(county);
			int y = item[0];
			int x = item[1];
			if(board[y][x] == 'M' ){
				board[y][x] = 'X';
				continue;
			}
			int count = 0;
			for (int i = 0; i < 8 ; i++) {
				int  ty= y  + dy[i];
				int  tx= x + dx[i];
				// 防止超限
				if(tx <0 || tx>= maxX || ty <0 || ty >= maxY){
					continue;
				}
				// 如果一个地雷（'M'）被挖出，游戏就结束了- 把它改为 'X'。
				// 如果一个没有相邻地雷的空方块（'E'）被挖出，修改它为（'B'），并且所有和其相邻的未挖出方块都应该被递归地揭露。
				// 如果一个至少与一个地雷相邻的空方块（'E'）被挖出，修改它为数字（'1'到'8'），表示相邻地雷的数量。
				// 如果在此次点击中，若无更多方块可被揭露，则返回面板。
				if(board[ty][tx] =='M'){
					count ++;
				}
			}
			if (count> 0 ){
				board[y][x] = (char) (count + 48);
			}else{
				board[y][x] = 'B';
			}
			if(board[y][x]> '0' && board[y][x] < '9'){
				continue;
			}
			for (int i = 0; i < 8 ; i++) {
				int  ty= y  + dy[i];
				int  tx= x + dx[i];
				// 防止超限
				if(tx <0 || tx>= maxX || ty <0 || ty >= maxY){
					continue;
				}
				// 广度优先
				int[] next = {ty, tx};
				if(board[ty][tx] == 'M' || (board[ty][tx]> '0' && board[ty][tx] < '9') || contains(queue,next)) {
					continue;
				}
				queue.add(next);
			}
		}
		return  board;
    }
    public  boolean contains(List<int[]> list,int[] item){
		for (int[] ints : list) {
			if(Arrays.equals(ints,item)){
				return true;
			}
		}
		return false;
	}
}
//leetcode submit region end(Prohibit modification and deletion)

}
