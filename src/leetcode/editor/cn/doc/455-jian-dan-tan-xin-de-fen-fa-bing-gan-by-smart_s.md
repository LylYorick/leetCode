## （1）算法思路

- 这道题是比较简单的**贪心问题**。
- 可以用以下**两个思路**来设计算法：
  - 给一个孩子的饼干 应当尽量小并且又能满足该孩子，这样大饼干才能拿来给满足度比较大的孩子。 
  - 满足度最小的孩子最容易得到满足，所以应先满足满足度最小的孩子。 
- 因此，可以编写下面的算法，得到**最优的分配方法**：
  - 将胃口数`g`数组和饼干大小数组`s`，进行**从小到大**的排序。
  - 从小到大开始**遍历两个数组**：
    - 如果当前`g[i]<=s[j]`，则可以满足该孩子的胃口，`result++`，`i++`,`j++`。
    - 否则，`j++`，看下一个饼干的大小能否满足对应孩子的胃口。
  - 当`i`或`j`到达对应数组的结尾时，跳出循环，返回结果值`result`。



## （2）复杂度分析

- **时间复杂度：O(mlogm)**，其中，m=max(n1,n2)，`n1`,`n2`分别为`g`，`s`数组的大小
  - sort算法的复杂度为O(nlogn)
  - while循环的复杂度是O(n1+n2)
  - 所以总的时间复杂度是O(mlogm)。
- **空间复杂度：O(1)**，只开辟了常数变量的大小



## （3）代码

```C++
int findContentChildren(vector<int>& g, vector<int>& s) {
	sort(g.begin(), g.end());
	sort(s.begin(), s.end());
	int i = 0, j = 0;
	int result = 0;
	while (i != g.size() && j != s.size())
	{
		if (g[i] <= s[j])
		{
			result++;
			i++;
			j++;
		}
		else
			j++;
	}
	return result;
}
```



## （4）截图

 [image.png](https://pic.leetcode-cn.com/546c2583e28d1d157a1411c504afb44bcd171954cc03f99a2f54c0bcabe0ba0c-image.png)

如果题解对你有启发或者帮助的话，不妨点个赞~
Thanks for your reading!!
