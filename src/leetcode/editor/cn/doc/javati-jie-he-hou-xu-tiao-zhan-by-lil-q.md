如果是匹配一个较短字符串 **s** ，对于 **s**中每一个`char` 都优先匹配最开始遇到的，直接扫描一遍 **t** 即可：

```java
class Solution {
    public boolean isSubsequence(String s, String t) {
        int i = 0;
        for (char ch : s.toCharArray()) {
            while (i < t.length() && t.charAt(i) != ch) i++;
            i++;
        }
        return i <= t.length();
    }
}
```
# 后续挑战

匹配一串字符需要 O(n) ，n 为 **t** 的长度。如果有大量输入的 **S**，称作 **S1** , **S2** , ... , **Sk** 其中 k >= 10 亿，你需要依次检查它们是否为 **T** 的子序列，这时候处理每一个子串都需要扫描一遍 **T** 是很费时的。

在这种情况下，我们需要在匹配前对 **T** 做预处理，利用一个二维数组记录每个位置的下一个要匹配的字符的位置，这里的字符是`'a' ~ 'z'`，所以这个数组的大小是 `dp[n][26]`，n 为 **T** 的长度。那么每处理一个子串只需要扫描一遍 **Si** 即可，因为在数组的帮助下我们对 **T** 是“跳跃”扫描的。比如下面匹配 "ada" 的例子，只需要“跳跃”三次。

 [20200426180603 .jpg](https://pic.leetcode-cn.com/dc770fff7b447043dc045a2efd3929c61f13b4fb491bd9c2e033340acb36d200-20200426180603%20.jpg)



```java
class Solution {
    public boolean isSubsequence(String s, String t) {
        // 预处理
        t = " " + t; // 开头加一个空字符作为匹配入口
        int n = t.length();
        int[][] dp = new int[n][26]; // 记录每个位置的下一个ch的位置
        for (char ch = 0; ch < 26; ch++) {
            int p = -1;
            for (int i = n - 1; i >= 0; i--) { // 从后往前记录dp
                dp[i][ch] = p;
                if (t.charAt(i) == ch + 'a') p = i;
            }
        }
        // 匹配
        int i = 0;
        for (char ch : s.toCharArray()) { // 跳跃遍历
            i = dp[i][ch - 'a'];
            if (i == -1) return false;
        }
        return true;
    }
}
```

这是我总结的一些[贪心算法题](https://lil-q.github.io/2020/04/22/%E8%B4%AA%E5%BF%83%E7%AE%97%E6%B3%95/#leetcode-392-%E5%88%A4%E6%96%AD%E5%AD%90%E5%BA%8F%E5%88%97)