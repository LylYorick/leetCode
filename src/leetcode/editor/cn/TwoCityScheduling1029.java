package leetcode.editor.cn;

//题目编号: 1029 题目名称: 两地调度

//公司计划面试 2N 人。第 i 人飞往 A 市的费用为 costs[i][0]，飞往 B 市的费用为 costs[i][1]。 
//
// 返回将每个人都飞到某座城市的最低费用，要求每个城市都有 N 人抵达。 
//
// 
//
// 示例： 
//
// 输入：[[10,20],[30,200],[400,50],[30,20]]
//输出：110
//解释：
//第一个人去 A 市，费用为 10。
//第二个人去 A 市，费用为 30。
//第三个人去 B 市，费用为 50。
//第四个人去 B 市，费用为 20。
//
//最低总费用为 10 + 30 + 50 + 20 = 110，每个城市都有一半的人在面试。
// 
//
// 
//
// 提示： 
//
// 
// 1 <= costs.length <= 100 
// costs.length 为偶数 
// 1 <= costs[i][0], costs[i][1] <= 1000 
// 
// Related Topics 贪心算法 
// 👍 115 👎 0

import java.util.*;

public class TwoCityScheduling1029{
	public static void main(String[] args) {
		Solution solution = new TwoCityScheduling1029().new Solution();
	//	System.out.println(solution.twoCitySchedCost(new int[][] {{10,20}, {30,200},{400,50},{30,20}}));
		System.out.println(solution.twoCitySchedCost(new int[][] {{259,770}, {448,54},{926,667}, {184,139}, {840,118},{577,469}}));
	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int twoCitySchedCost(int[][] costs) {
		TreeMap<Integer,Integer> map = new TreeMap<>();
		for (int i = 0; i < costs.length; i++) {
//			System.out.println(Math.abs(costs[i][0] - costs[i][1]));
			map.put(i,Math.abs(costs[i][0] - costs[i][1]));
		}
		List<Map.Entry<Integer,Integer>> list = new ArrayList<Map.Entry<Integer,Integer>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
			@Override
			public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
				return o2.getValue() - o1.getValue();
			}
		});
		//list.lastEntry().getValue();
		int sum = 0;
		int count_0 = 0;
		int count_1 = 0;
		int half= costs.length/2;
		for (int i = 0; i < list.size() ; i++) {
		  int [] item = costs[list.get(i).getKey()];
			//System.out.print(item[0] + " " + item[1]+ " ");
			if(count_0 <half && count_1 < half){
				if(item[0] > item[1]){
					//System.out.println(item[1]);
					sum += item[1];
					count_1 ++;
				}else{
					System.out.println(item[0]);
					sum += item[0];
					count_0 ++ ;
				}
			} else if(count_0 >= half){
				sum += item[1];
				count_1 ++;
			} else{
				sum += item[0];
				count_1 ++ ;
			}


		}

		return sum;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
