package leetcode.editor.cn;

import java.util.Scanner;
//汽水瓶问题
public class Pressurizedbottle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){
            int num = scanner.nextInt();
            if(num ==0) return;
            test(num)
            ;
        }
    }
    public static  void test(int num){
        int result = 0;
        while(num > 1){
            if(num == 2){
                result ++;
                break;
            }
            int changeNum = num / 3;
            num = changeNum + num % 3;
            result += changeNum;
        }
        System.out.println(result);
    }
}
