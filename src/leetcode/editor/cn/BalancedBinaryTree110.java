package leetcode.editor.cn;

//题目编号: 110 题目名称: 平衡二叉树

//给定一个二叉树，判断它是否是高度平衡的二叉树。 
//
// 本题中，一棵高度平衡二叉树定义为： 
//
// 
// 一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过1。 
// 
//
// 示例 1: 
//
// 给定二叉树 [3,9,20,null,null,15,7] 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
//
// 返回 true 。 
// 
//示例 2: 
//
// 给定二叉树 [1,2,2,3,3,null,null,4,4] 
//
//        1
//      / \
//     2   2
//    / \
//   3   3
//  / \
// 4   4
// 
//
// 返回 false 。 
//
// 
// Related Topics 树 深度优先搜索 
// 👍 422 👎 0

import java.util.Stack;
import java.util.TreeMap;

public class BalancedBinaryTree110{
	public static void main(String[] args) {
		Solution solution = new BalancedBinaryTree110().new Solution();
//		TreeNode root = new BalancedBinaryTree110().new TreeNode(3);
//		TreeNode treeNode1 = new BalancedBinaryTree110().new TreeNode(9);
//		TreeNode treeNode2 = new BalancedBinaryTree110().new TreeNode(20);
//		root.left = treeNode1;
//		root.right = treeNode2;
//		TreeNode treeNode3 = new BalancedBinaryTree110().new TreeNode(9);
//		TreeNode treeNode4 = new BalancedBinaryTree110().new TreeNode(20);
//		treeNode2.left = treeNode3;
//		treeNode2.right = treeNode4;
//		System.out.println(solution.isBalanced(root));



//		TreeNode root = new BalancedBinaryTree110().new TreeNode(1);
//		TreeNode treeNode1 = new BalancedBinaryTree110().new TreeNode(2);
//		TreeNode treeNode2 = new BalancedBinaryTree110().new TreeNode(2);
//		root.left = treeNode1;
//		root.right = treeNode2;
//		TreeNode treeNode3 = new BalancedBinaryTree110().new TreeNode(3);
//		TreeNode treeNode4 = new BalancedBinaryTree110().new TreeNode(3);
//		treeNode1.left = treeNode3;
//		treeNode1.right = treeNode4;
//		TreeNode treeNode5 = new BalancedBinaryTree110().new TreeNode(4);
//		TreeNode treeNode6 = new BalancedBinaryTree110().new TreeNode(4);
//		treeNode3.left = treeNode5;
//		treeNode3.right = treeNode6;
//		System.out.println(solution.isBalanced(root));
		TreeNode root = new BalancedBinaryTree110().new TreeNode(1);
//		TreeNode treeNode1 = new BalancedBinaryTree110().new TreeNode(2);
		TreeNode treeNode2 = new BalancedBinaryTree110().new TreeNode(2);

		root.right = treeNode2;

		TreeNode treeNode3 = new BalancedBinaryTree110().new TreeNode(3);
		treeNode2.right = treeNode3;

		System.out.println(solution.isBalanced(root));

	}
//leetcode submit region begin(Prohibit modification and deletion)

 //Definition for a binary tree node.
 public class TreeNode {
     int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
 }

class Solution {
//    public boolean isBalanced(TreeNode root) {
//		Stack<TreeNode> stack =new Stack<>();
//		stack.push(root);
//		int deep = 0;
//		while(!stack.isEmpty()){
//			TreeNode item = stack.peek();
//			if(item.right)
//			if(item.left == null && item.right != null){
//				deep++;
//			}else  if(item.left != null && item.right == null){
//				deep --;
//			}
//			if(Math.abs(deep) > 1){
//				return  false;
//			}
//			if()
//
//		}
//    }

	public boolean isBalanced(TreeNode root) {
		if(root == null){
			return  true;
		}
		int deep = countDeep(root);
		if (deep == 1000){
			return false;
		}
		return  true;
	}
	public int countDeep(TreeNode node){
		int leftDeep = node.left == null ? 0 : countDeep(node.left);
		if(leftDeep == 1000){
			return 1000;
		}
		int rightDeep = node.right  == null ? 0 : countDeep(node.right);
		if(rightDeep == 1000){
			return 1000;
		}
		if(Math.abs(leftDeep - rightDeep) > 1){
			return 1000;
		}else {
			return 1 + Math.max(leftDeep,rightDeep);
		}
	}
}
//leetcode submit region end(Prohibit modification and deletion)

}
