package leetcode.editor.cn;

//题目编号: 1005
//题目名称: K 次取反后最大化的数组和
//给定一个整数数组 A，我们只能用以下方法修改该数组：我们选择某个索引 i 并将 A[i] 替换为 -A[i]，然后总共重复这个过程 K 次。（我们可以多次选
//择同一个索引 i。） 
//
// 以这种方式修改数组后，返回数组可能的最大和。 
//
// 
//
// 示例 1： 
//
// 输入：A = [4,2,3], K = 1
//输出：5
//解释：选择索引 (1,) ，然后 A 变为 [4,-2,3]。
// 
//
// 示例 2： 
//
// 输入：A = [3,-1,0,2], K = 3
//输出：6
//解释：选择索引 (1, 2, 2) ，然后 A 变为 [3,1,0,2]。
// 
//
// 示例 3： 
//
// 输入：A = [2,-3,-1,5,-4], K = 2
//输出：13
//解释：选择索引 (1, 4) ，然后 A 变为 [2,3,-1,5,4]。
// 
//
// 
//
// 提示： 
//
// 
// 1 <= A.length <= 10000 
// 1 <= K <= 10000 
// -100 <= A[i] <= 100 
// 
// Related Topics 贪心算法 
// 👍 51 👎 0

import java.util.*;

public class MaximizeSumOfArrayAfterKNegations{
	public static void main(String[] args) {
		Solution solution = new MaximizeSumOfArrayAfterKNegations().new Solution();
//		System.out.println(solution.largestSumAfterKNegations(new int[]{4,2,3},1));;
//		System.out.println(solution.largestSumAfterKNegations(new int[]{3,-1,0,2},3));
//		System.out.println(solution.largestSumAfterKNegations(new int[]{2,-3,-1,5,-4},2));
		System.out.println(solution.largestSumAfterKNegations(new int[]{-88,-40,85,-40,2,-45,1,47,89,19,-27,40,-6,-39,40,-19,35,87,88,-37,31,-79,33,8,-2,56,25,16,-60,-9,-7,-23,-24,86,-79,79,80,-69,10,-21,-93,-25,23,-59,-81,-50,-2,-46,-64,-91,82,25,24,8,-59,53,-94,61,-18,-67,47,34,77,11,11,-81,84,29,-61,-12,-94,41,-56,-1,-79,10,-32,67,17,45,-11,-4,44,66,-98,-55,67,43,-28,-80,72,-97,-86,-99,1,43,-75,-72,-24,-92,-42,-44,38,33,-64,-12,-82,-60,38,-51,71,-47,40,42,-85,60,-46,-61,-25,17,-13,-17,21,84,-56,-72,95,67,-28,73,53,-4,-14,-92,21,-43,82,-63,-98,42,65,-97,-78,72,54,65,44,-15,-88,7,23,-62,-8,-6,-11,-93,43,81,28,-58,-67,-62,-53,-19,-89,-46,-51,61,58,-96,71,43,-84,65,54,94,33,-58,79,-38,-97,-67,18,-20,68,97,32,35,34,46,4,-62,97,-12,57,-69,-87,54,76,-82,16,4,-36,68,-51,4,-66,51,-6,-13,37,-75,85,86,52,44,15,10,58,-4,-93,42,5,-69,72,-69,-24,-79,-63,-64,-95,-100,-28,-93,100,20,27,17,-42,-48,-40,46,-100,-56,90,41,23,-90,98,78,66,-77,-95,-85,-24,77,-8,47,-62,-82,-38,-66,-89,38,100,-65,91,-78,76,-15,44,95,-77,-34,-30,-28,-44,-54,-60,51,-4,75,-96,-76,75,-54,-18,14,-80,3,98,42,-52,54,57,12,76,45,-21,90},187));

	}
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
	//最好最快的解法，通过使用 number 数组，将各数字出现次数存入其中，可不排序就快速找到最小值。
    public int largestSumAfterKNegations(int[] A, int K) {
		int number[] = new int[201];//-100 <= A[i] <= 100,这个范围的大小是201
		for (int i = 0; i < A.length; i++) {
			number[A[i] + 100]++;//将[-100,100]映射到[0,200]上
		}
		int i = 0;
		while(K > 0 ){
			while(number[i] == 0){
				i++;
			}
			number[i] --;
			number[200 -i]++;
			if( i > 100){
				i = 200 -i;
			}
			K--;
		}
		int sum = 0;
		for (int j = 0; j < number.length; j++) {
			sum = sum + (number[j] * (j - 100));
		}
		return sum;

    }
	//最初始的想法
	public int largestSumAfterKNegations2(int[] A, int K) {
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < A.length; i++) {
			list.add(A[i]);
		}

		int max = 0;
		for (int i = 0; i < K; i++) {
			Collections.sort(list);
			list.set(0,-list.get(0));
		}
		int sum = 0;
		for (Integer item : list) {
			sum +=item;
		}
		return  sum;
	}

}
//leetcode submit region end(Prohibit modification and deletion)

}
