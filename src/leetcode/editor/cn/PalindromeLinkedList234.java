package leetcode.editor.cn;

//题目编号: 234 题目名称: 回文链表

//请判断一个链表是否为回文链表。 
//
// 示例 1: 
//
// 输入: 1->2
//输出: false 
//
// 示例 2: 
//
// 输入: 1->2->2->1
//输出: true
// 
//
// 进阶： 
//你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？ 
// Related Topics 链表 双指针 
// 👍 727 👎 0

import java.util.Stack;

public class PalindromeLinkedList234{
	public static void main(String[] args) {
		Solution solution = new PalindromeLinkedList234().new Solution();
		ListNode head = new  PalindromeLinkedList234().new ListNode(1,
				new  PalindromeLinkedList234().new ListNode(2));
//		System.out.println(solution.isPalindrome(head));


		 head = new  PalindromeLinkedList234().new ListNode(1,
				new  PalindromeLinkedList234().new ListNode(2,
						new  PalindromeLinkedList234().new ListNode(2,
								new  PalindromeLinkedList234().new ListNode(1
								))));
		System.out.println(solution.isPalindrome(head));


	}
//leetcode submit region begin(Prohibit modification and deletion)

  public class ListNode {
     int val;
      ListNode next;
      ListNode(int x) { val = x; }
	  ListNode(int val,  ListNode next) { this.val = val; this.next = next; }
  }
class Solution {
    public boolean isPalindrome(ListNode head) {
		Stack<ListNode> stack = new Stack<>();
		ListNode	node = head;

		while (node != null){
			stack.push(node);
			node = node.next;
		}
		node = head;

		while(!stack.isEmpty()){
			ListNode pop = stack.pop();
			if(pop.val != node.val){
				return false;
			}
			node = node.next;
		}
		return true;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}
